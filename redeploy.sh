#!/bin/bash

PROJECT_NAME=deps-analyzer
PROJECT_DIR=`pwd`
WAR_LOCATION=$PROJECT_DIR/target/$PROJECT_NAME.war
TOMCAT_HOME=/home/dimas/install/tomcat-7.0.75
#TOMCAT_HOME=/home/dimas/programs/apache-tomcat-7.0.64
TOMCAT_WEBAPPS=$TOMCAT_HOME/webapps

printf $WAR_LOCATION

printf "\nBUILDING WAR FILE (skip tests)"
mvn clean install -DskipTests=true

printf "\nREMOVING '$PROJECT_NAME' FILES FROM TOMCAT"
rm -rf $TOMCAT_WEBAPPS/$PROJECT_NAME*

printf "\nCOPYING '$PROJECT_NAME.war' IN TOMCAT"
cp $WAR_LOCATION $TOMCAT_WEBAPPS

printf "\nCHECKING IF TOMCAT HAS ALREADY STARTED"
TOMCAT_PID=`lsof -i -n -P | grep '8080 (LISTEN)' | awk '{print $2}'`
if [ ! -z "$TOMCAT_PID" ]; then
  printf "\nTOMCAT IS STARTED: $TOMCAT_PID"
else
  printf "\nTOMCAT IS DOWN. GOING TO START IT"
  sh $TOMCAT_HOME/bin/startup.sh
fi

# link front-end files in tomcat to project files
if [ "$1" = "link" ]; then
	printf "" > $TOMCAT_HOME/logs/catalina.out
	printf "\ngoing to link 'js', 'views' directories"
	
	tail -f $TOMCAT_HOME/logs/catalina.out | while read LOGLINE
	do
	   [[ "${LOGLINE}" == *"$PROJECT_NAME.war has finished"* ]] && pkill -P $$ tail
	done
	
	rm -rf $TOMCAT_WEBAPPS/$PROJECT_NAME/js/appControllers.js
	ln -s $PROJECT_DIR/src/main/webapp/js/appControllers.js $TOMCAT_WEBAPPS/$PROJECT_NAME/js/appControllers.js
	rm -rf $TOMCAT_WEBAPPS/$PROJECT_NAME/js/app.js
	ln -s $PROJECT_DIR/src/main/webapp/js/app.js $TOMCAT_WEBAPPS/$PROJECT_NAME/js/app.js
	
	rm -rf $TOMCAT_WEBAPPS/$PROJECT_NAME/views
	ln -s $PROJECT_DIR/src/main/webapp/views $TOMCAT_WEBAPPS/$PROJECT_NAME/views

fi

#kill -9 $(lsof -i -n -P | grep '8080 (LISTEN)' | awk '{print $2}')
