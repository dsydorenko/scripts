#!/bin/sh

# STEP.1: update JAVA_HOME variable in.bashrc file:
export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_45
#export JAVA_HOME=/usr/lib/jvm/java-7-oracle/jre

# STEP.2: update java alternatives
sudo update-alternatives --config java

# STEP.3: update javac alternatives
sudo update-alternatives --config javac