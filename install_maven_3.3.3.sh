#!/bin/sh

MAVEN_DIR="$HOME/install/maven_3.3.3"
MAVEN_ARCHIVE="apache-maven-3.3.3-bin.tar.gz"

if [ ! -d "$MAVEN_DIR" ]; then
  echo "No maven directory detected. Creating new one and downloading maven archive..."
  mkdir $MAVEN_DIR
  wget http://apache.volia.net/maven/maven-3/3.3.3/binaries/$MAVEN_ARCHIVE -P $MAVEN_DIR
  tar -xvzf $MAVEN_DIR/$MAVEN_ARCHIVE -C $MAVEN_DIR
  rm $MAVEN_DIR/$MAVEN_ARCHIVE
fi

echo "Maven run command : $MAVEN_DIR/apache-maven-3.3.3/bin/mvn"
