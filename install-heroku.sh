# Run this from your terminal:
# Please ensure that you have Ruby installed.
wget -O- https://toolbelt.heroku.com/install-ubuntu.sh | sh

# Log in using the email address and password you used when creating your Heroku account
heroku login

# Prepare a simple application that can be deployed
git clone https://github.com/<PATH_TO_REPO>.git
cd <REPO_NAME>

# Create an app on Heroku
heroku apps:create <APP_NAME>

# Now deploy your code
git push heroku master

# Ensure that at least one instance of the app is running
heroku ps:scale web=1

# Open tab in browser
heroku open

# View logs
heroku logs --tail
